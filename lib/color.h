#ifndef COLOR_H_INCLUDED
#define COLOR_H_INCLUDED

#ifdef __WIN32__
#include <windows.h>
#elif __WIN62__ || __linux
#error "belum support Ke Sistem ini"
#endif // __WIN32__

#define _RED_ SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),12);
#define _BLUE_ SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),9);
#define _GREEN_ SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),10);
#define _AQUA_ SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),11);
#define _PURPLE_ SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),13);
#define _YELLOW_ SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),14);
#define _DEFAULT_ SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),15);

#endif // COLOR_H_INCLUDED
